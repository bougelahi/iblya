package com.bouge.vktool

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.kek.view.*

class DenisHolder(item: View): RecyclerView.ViewHolder(item)

class DenAdapter(private val list: ArrayList<Int>, private val con: Context): RecyclerView.Adapter<DenisHolder>() {


    override fun onViewDetachedFromWindow(holder: DenisHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.jipeg.clearAnimation()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DenisHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.kek, parent, false)
        return DenisHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: DenisHolder, position: Int) {
        if (position % 2 == 0) setAnim(holder.itemView.jipeg)
        holder.itemView.number.text = position.toString()
    }

    private fun setAnim(view: View){

        val anim = AnimationUtils.loadAnimation(con, R.anim.denis)
        view.startAnimation(anim)
    }

}