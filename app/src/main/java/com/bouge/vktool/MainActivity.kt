package com.bouge.vktool

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = ArrayList<Int>()

        for (i in 0..100) list.add(i)

        val adapter = DenAdapter(list, applicationContext)

        recycler.apply {
            this.adapter = adapter
            layoutManager = LinearLayoutManager(context)

        }

    }
}
